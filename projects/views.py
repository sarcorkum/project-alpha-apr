from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProject


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    projects = get_object_or_404(Project, id=id)
    context = {
        "projects_object": projects,
        "list_projects": projects,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


def search(request):
    if request.method == "POST":
        searched = request.POST["searched"]
        projects = Project.objects.filter(name__contains=searched)
        return render(
            request,
            "projects/search.html",
            {"searched": searched, "projects": projects},
        )
    else:
        return render(request, "projects/search.html", {})
